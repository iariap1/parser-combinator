import string


class ParserException(Exception):
    pass


def letter():
    "letter(): matches any alphabetic character,"
    return one_of(string.ascii_letters)


def space():
    "space(): matches any whitespace character,"
    return one_of(string.whitespace)


def spaces():
    "matches multiple whitespace characters,"
    return many(space())


def digit():
    "matches any digit"
    return one_of(string.digits)


def many(parser):
    "many(p): Repeat parser p from never to infinitely many times"
    def __many(input, pos):
        answer = []
        while pos < len(input):
            try:
                xs, pos = parser(input, pos)
                answer.append(xs)
            except ParserException as pe:
                break
        return answer, pos
    return __many


def pmap(parser, f):
    "maps parser output to f"
    def __pmap(input, pos):
        xs, pos = parser(input, pos)
        return f(xs), pos
    return __pmap


def one_of(str):
    "matches any character from the provided string"
    def __one_of(input, pos):
        try:
            xs = input[pos]
            if xs in set(str):
                return xs, pos + 1
        except:
            pass
        raise ParserException(f"Expected one of '{str}' at position {pos}")

    return __one_of


def none_of(str):
    "match characters which are not in the provided string."
    def __none_of(input, pos):
        xs = input[pos]
        if not xs in str:
            return xs, pos + 1
        else:
            raise ParserException(f"Found one of '{str}' at position {pos}")
    return __none_of


def pstring(str):
    "matches any specified string str"
    def __string(input, pos):
        try:
            end = pos + len(str)
            xs = input[pos:end]
            if xs == str:
                return xs, end
        except:
            pass
        raise ParserException(f"Expected '{str}' at position {pos}")
    return __string


def choice(*args):
    """ 
        |: This combinator implements choice. The parser p | q first applies p. If it succeeds, 
        the value of p is returned. If p fails without consuming any input, parser q is tried. NOTICE: without backtrack,"
    """
    def __choice(input, pos):
        e = []
        for parser in args:
            try:
                return parser(input, pos)
            except ParserException as pe:
                e.append(pe)
        raise ParserException(e)
    return __choice


def joint(*args):
    "+: Joint two or more parsers into one. Return the aggregate of two results from this two parser."
    def __joint(input, pos):
        answer = []
        for parser in args:
            xs, pos = parser(input, pos)
            answer.append(xs)
        return answer, pos
    return __joint


def sepBy(chunk, sep):
    "sepBy(p, sep) parses zero or more occurrences of p, separated by sep. Returns a list of values returned by p."
    """ 
        Many(chunk + sep) + chunk
        x[0][0], x[0][1], x[0][2]
    """
    return pmap(
        joint(
            pmap(many(joint(chunk, sep)), lambda l: [x[0] for x in l]),
            chunk
        ),
        lambda xs: [*xs[0], xs[1]])


def between(left, contents, right):
    def __between(input, pos):
        (_, pos) = left(input, pos)
        (answer, pos) = contents(input, pos)
        (_, pos) = right(input, pos)
        return answer, pos
    return __between


def parse(parser, text, index=0):
    "parses the given text starting at index"
    return parser(text, index)

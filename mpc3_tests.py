import string
import unittest
from mpc3 import ParserException, between, choice, digit, joint, letter, many, none_of, one_of, parse, pmap, pstring, sepBy, space, spaces


class TestLetterParsing(unittest.TestCase):

    def setUp(self) -> None:
        self.parser = letter()
        return super().setUp()

    def test_matching(self):
        xs, pos = parse(self.parser, "Hola", 0)
        self.assertEqual(pos, 1)
        self.assertEqual(xs, "H")

    def test_matching_many(self):
        st = "Hola"
        xs, pos = parse(many(self.parser), st, 0)
        self.assertEqual(pos, len(st))
        self.assertEqual("".join(xs), st)

    def test_not_matching(self):
        with self.assertRaises(ParserException):
            xs, pos = parse(self.parser, "123", 0)


class TestSpaceParsing(unittest.TestCase):

    def setUp(self) -> None:
        self.parser = space()
        return super().setUp()

    def test_matching(self):
        for ch in string.whitespace:
            xs, pos = parse(self.parser, ch, 0)
            self.assertEqual(pos, 1)
            self.assertEqual(xs, ch)

    def test_not_matching(self):
        with self.assertRaises(ParserException):
            xs, pos = parse(self.parser, "123", 0)


class TestSpacesParsing(unittest.TestCase):

    def setUp(self) -> None:
        self.parser = pmap(spaces(), lambda x: "".join(x))
        return super().setUp()

    def test_matching(self):
        xs, pos = parse(self.parser, "      end-of-spaces", 0)
        self.assertEqual(pos, 6)
        self.assertEqual(xs, "      ")

    def test_not_matching(self):
        xs, pos = parse(self.parser, "123", 0)
        self.assertEqual(pos, 0)


class TestOneOfParsing(unittest.TestCase):

    def setUp(self) -> None:
        self.parser = one_of("-+")
        return super().setUp()

    def test_matching(self):
        xs, pos = parse(self.parser, "-10", 0)
        self.assertEqual(pos, 1)
        self.assertEqual(xs, "-")

    def test_not_matching(self):
        with self.assertRaises(ParserException):
            xs, pos = parse(self.parser, "123", 0)


class TestNoneOfParsing(unittest.TestCase):

    def setUp(self) -> None:
        self.parser = none_of("-+")
        return super().setUp()

    def test_matching(self):
        xs, pos = parse(self.parser, "10", 0)
        self.assertEqual(pos, 1)
        self.assertEqual(xs, "1")

    def test_not_matching(self):
        with self.assertRaises(ParserException):
            xs, pos = parse(self.parser, "-123", 0)


class TestDigitParsing(unittest.TestCase):

    def setUp(self) -> None:
        self.parser = digit()
        return super().setUp()

    def test_matching(self):
        xs, pos = parse(self.parser, "1", 0)
        self.assertEqual(pos, 1)
        self.assertEqual(xs, "1")

    def test_matching_many(self):
        xs, pos = parse(many(self.parser), "1234a", 0)
        self.assertEqual(pos, 4)
        self.assertEqual("".join(xs), "1234")

    def test_matching_many_maped(self):
        xs, pos = parse(
            pmap(
                many(self.parser),
                lambda x: int("".join(x))
            ), "1234a", 0)
        self.assertEqual(pos, 4)
        self.assertEqual(xs, 1234)

    def test_not_matching(self):
        with self.assertRaises(ParserException):
            xs, pos = parse(self.parser, "a", 0)


class TestStringParsing(unittest.TestCase):

    def setUp(self) -> None:
        self.parser = pstring("comienzo")
        return super().setUp()

    def test_matching_many(self):
        with self.assertRaises(ParserException):
            xs, pos = parse(self.parser, "1", 0)
            self.assertEqual(pos, 1)
            self.assertEqual(xs, "1")

    def test_matching(self):
        xs, pos = parse(self.parser, "comienzo de una frase", 0)
        self.assertEqual(pos, 8)
        self.assertEqual(xs, "comienzo")


class TestJointParsing(unittest.TestCase):

    def setUp(self) -> None:
        self.parser = joint(pstring("hola"), spaces(), pstring("mundo"))
        return super().setUp()

    def test_matching(self):
        xs, pos = parse(self.parser, "hola mundo", 0)
        self.assertEqual(pos, 10)
        self.assertEqual(len(xs), 3)


class TestChoiceParsing(unittest.TestCase):

    def setUp(self) -> None:
        self.parser = choice(pstring("hola"), digit())
        return super().setUp()

    def test_matching(self):
        xs, pos = parse(self.parser, "1a", 0)
        self.assertEqual(pos, 1)
        self.assertEqual(xs, "1")

    def test_matching2(self):
        xs, pos = parse(self.parser, "hola1", 0)
        self.assertEqual(pos, 4)
        self.assertEqual(xs, "hola")

    def test_not_matching(self):
        with self.assertRaises(ParserException):
            xs, pos = parse(self.parser, "-", 0)


class TestSQLQueryParsing(unittest.TestCase):
    """ 
        columns = '*' | sepBy(word, ',') | word 
        select = select columns
        from = tablename + ',' | tablename
        query = select + from 
    """

    def setUp(self) -> None:
        #  [['select', ' ', ['nombre', 'apellido']], ' ', ['from', ' ', ['tabla']]]        
        word = pmap(many(letter()), lambda x: "".join(x))

        self.columns = choice(
            one_of('*'),
            sepBy(word, one_of(',')),
            word
        )

        self.select = pmap(joint(
            pstring('select'),
            space(),
            self.columns
        ), lambda x : x[0] + ': <' + " and ".join(x[2]) + ">" )

        self.from_ = joint(
            pstring("from"),
            space(),
            choice(
                sepBy(word, one_of(',')),
                word
            )
        )
        self.parser = joint(self.select, space(), self.from_)
        return super().setUp()

    def test_matching(self):
        query = "select nombre,apellido from tabla"
        xs, pos = parse(self.parser, query)
        print(xs)
        self.assertEqual(pos, len(query))

#        self.assertEqual(pos, 1)
#        self.assertEqual(xs, "1")


class TestSepByParsing(unittest.TestCase):

    def setUp(self) -> None:
        word = pmap(many(letter()), lambda x: "".join(x))
        self.parser = sepBy(word, one_of(","))
        return super().setUp()

    def test_matching(self):
        st = "hola,como,te,va"
        xs, pos = parse(self.parser, st)
        self.assertEqual(pos, len(st))
        self.assertEqual(len(xs), 4)


class TestJsonParsing(unittest.TestCase):
    """ 
        object = "{" + properties + "}"
        properties = many(property + ',') | property | ""
        property = word + ":" + propertyValue
        propertyValue = object | array | string | number
    """

    def word(self):
        return pmap(many(letter()), lambda x: "".join(x))

    def string_(self):
        """ 
            string = " + word + " | ' + word + '
        """
        word = self.word()
        return choice(
            between(one_of('"'), word, one_of('"')),
            between(one_of('\''), word, one_of('\'')),
        )

    def number_(self):
        return pmap(
            many(digit()),
            lambda x: int("".join(x))
        )

    def boolean_(self):
        return pmap(
                choice(
                    pstring("true"),
                    pstring("false"),
                ),
                lambda x : True if x == 'true' else False
        )

    def property_(self):
        return joint(self.word(), one_of(":"), self.propertyValue())

    def properties(self):
        """
        properties = many(property + ',') | property | ""
        """
        return choice(
            sepBy(
                self.property_(),
                one_of(',')
            ),
            self.property_()
        )

    def object_(self):
        """
            object = { properties }  |{} 
        """
        return choice(
            between(
                one_of("{"),
                self.properties(),
                one_of("}")
            ),
            pmap(joint(one_of('{'), one_of('}')), lambda x:  None)
        )

    def array_(self):
        """
            array = [ propertyValue + ',' ]  | [ propertyValue ] |[] 
        """
        return choice(
            pmap(joint(one_of('['), one_of(']')), lambda x: []),
            between(
                one_of("["),
                sepBy(self.propertyValue(), one_of(',')),
                one_of("]")
            ),
            between(
                one_of("["),
                self.propertyValue(),
                one_of("]")
            )
        )

    def propertyValue(self):
        def propertyValue_(input, pos):
            return choice(
                self.object_(),
                self.array_(),
                self.boolean_(),
                self.string_(),
                self.number_(),
            )(input, pos)
        return propertyValue_

    def setUp(self) -> None:
        self.parser = self.object_()
        return super().setUp()

    def test_emptyobj(self):
        st = "{}"
        xs, pos = parse(self.parser, st)
        self.assertEqual(pos, len(st))
        self.assertIsNone(xs)

    def test_string_property(self):
        st = "name:'John'"
        xs, pos = parse(self.property_(), st)
        self.assertEqual(pos, len(st))
        self.assertEqual(xs[0], "name")
        self.assertEqual(xs[1], ":")
        self.assertEqual(xs[2], "John")

    def test_number_properties(self):
        st = "{name:10}"
        parser = self.object_()
        xs, pos = parse(parser, st)
        self.assertTrue(xs[0][0], 'name')
        self.assertTrue(xs[0][0], ':')
        self.assertTrue(xs[0][2], 10)

    def test_string_obj(self):
        st = "{name:'John'}"
        xs, pos = parse(self.parser, st)
        self.assertEqual(pos, len(st))
        prop = xs[0]
        self.assertEqual(prop[0], "name")
        self.assertEqual(prop[1], ":")
        self.assertEqual(prop[2], "John")

    def test_empty_array(self):
        st = "[]"
        parser = self.array_()
        xs, pos = parse(parser, st)
        self.assertEqual(pos, len(st))
        self.assertEqual(len(xs), 0)

    def test_string_array(self):
        st = "['hola','pato']"
        parser = self.array_()
        xs, pos = parse(parser, st)
        self.assertEqual(pos, len(st))
        self.assertEqual(xs[0], 'hola')
        self.assertEqual(xs[1], 'pato')

    def test_number_array(self):
        st = "{list:[1,2]}"
        xs, pos = parse(self.parser, st)
        self.assertEqual(pos, len(st))
        answer = xs[0]
        self.assertEqual(len(answer), 3)
        self.assertEqual(answer[0], 'list')
        self.assertEqual(answer[1], ':')
        l = answer[2]
        self.assertEqual(len(l), 2)
        self.assertEqual(l[0], 1)
        self.assertEqual(l[1], 2)

    def test_boolean(self):
        st = "{bool:true}"
        xs, pos = parse(self.parser, st)
        self.assertEqual(pos, len(st))
        answer = xs[0]
        self.assertEqual(len(answer), 3)
        self.assertEqual(answer[0], 'bool')
        self.assertEqual(answer[1], ':')
        self.assertEqual(answer[2], True)

if __name__ == '__main__':
    unittest.main()

## Implemented some features from parsec just for fun:

For the sake of convenience, we first define the characters we wish to match. parsec provides many types:

- letter(): matches any alphabetic character,

- string(str): matches any specified string str,

- space(): matches any whitespace character,

- spaces(): matches multiple whitespace characters,

- digit(): matches any digit,

eof(): matches EOF flag of a string,

regex(pattern): matches a provided regex pattern,

- one_of(str): matches any character from the provided string,

- none_of(str): match characters which are not in the provided string.

We can separate them with operators, according to the docs:

- |: This combinator implements choice. The parser p | q first applies p. If it succeeds, the value of p is returned. If p fails without consuming any input, parser q is tried. NOTICE: without backtrack,

- +: Joint two or more parsers into one. Return the aggregate of two results from this two parser.

^: Choice with backtrack. This combinator is used whenever arbitrary look ahead is needed. The parser p || q first applies p, if it success, the value of p is returned. If p fails, it pretends that it hasn't consumed any input, and then parser q is tried.

<<: Ends with a specified parser, and at the end parser consumed the end flag,

<: Ends with a specified parser, and at the end parser hasn't consumed any input,

>>: Sequentially compose two actions, discarding any value produced by the first,

mark(p): Marks the line and column information of the result of the parser p.

Then there are multiple "combinators":

times(p, mint, maxt=None): Repeats parser p from mint to maxt times,

count(p,n): Repeats parser p n-times. If n is smaller or equal to zero, the parser equals to return empty list,

(p, default_value=None): Make a parser optional. If success, return the result, otherwise return default_value silently, without raising any exception. If default_value is not provided None is returned instead,

- many(p): Repeat parser p from never to infinitely many times,

many1(p): Repeat parser p at least once,

separated(p, sep, mint, maxt=None, end=None): ,

- sepBy(p, sep): parses zero or more occurrences of parser p, separated by delimiter sep,

sepBy1(p, sep): parses at least one occurrence of parser p, separated by delimiter sep,

endBy(p, sep): parses zero or more occurrences of p, separated and ended by sep,

endBy1(p, sep): parses at least one occurrence of p, separated and ended by sep,

sepEndBy(p, sep): parses zero or more occurrences of p, separated and optionally ended by sep,

sepEndBy1(p, sep): parses at least one occurrence of p, separated and optionally ended by sep.

http://www.valuedlessons.com/2008/02/pysec-monadic-combinatoric-parsing-in.html
